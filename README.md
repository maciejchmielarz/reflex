# *ReFlex Project*

# Overview
## Using Gmail As Universal Client

I use Gmail cloud service as my primary client for vast majority of e-mail communication - either private or for various businesses that I'm involved in. This means I have several other addresses associated with it. I have a specific setup for this which I developed over the years. It boils down to every external account having set up:

* Message forwarding to my Gmail address (for incoming mail).
* SMTP access in Gmail (for outgoing mail).

You may ask, why message forwarding and not POP3? The latter doesn't work for me well because it causes a random delay before the message actually appears in Gmail as it uses some strange algorithm to determine mail checking intervals for each account separately. I fallback to POP3 only if message forwarding option is not exposed in external account's web client.

## Standard Autoresponder Falls Short

There's one thing that works extremely bad in this setup though: autoresponder.

Standard Gmail's "vacation reply" acts only on messages sent directly to @gmail.com address which is completely unacceptable considering my situation. When looking for a solution I also tried "canned messages" from Gmail Laboratory and was able to make them work quite well but they have another unacceptable drawback: don't allow to limit the number of messages sent to particular addresses which effectively means bombarding people with autoreplies throughout the whole unavailability period.

## Building Custom Autoresponder Is Fun

When looking for a solution to my problem I stumbled upon [this blog post](https://www.pygaze.org/2015/06/selective-gmail-autoreply/) by [Edwin Dalmaijer](https://twitter.com/esdalmaijer/). He seemed to struggle with a similar problem and found his solution with Google Apps Script. I quickly learned what it is:

> Google Apps Script is a JavaScript cloud scripting language that provides easy ways to automate tasks across Google products and third party services and build web applications.

...and instantaneously decided to develop a custom autoresponder with it :) After a bit of coding I came up with a solution that worked really nice so I decided to share it with others under the name ReFlex (which is a wordplay on bouncing back or Responding Flexibly).

## Stay Offline And Keep Things Under Control

ReFlex setup solves not only the problem of responding to mail sent to non-Gmail addresses. It also helps to stay really offline as it suppresses Gmail notifications by archiving all incoming messages and labeling them for easy reference after coming back. Only messages sent to address with a suffix "+urgent" are not archived and cause the notification to appear. That's why the automatic reply contains a following paragraph:

> If the matter is really urgent and requires my reaction before that date, please send a message to maciej.chmielarz+urgent@gmail.com. Thank you for your understanding.

This way you can avoid being disturbed by ordinary message flow but still provide people with a way to reach you in case things get really hot.

# User's Manual
## Initial Setup
### Google Apps Script
1. Go to https://script.google.com to create a new Google Apps Script project.
2. Create `autoRespond.gs` and `responderDates.gs` together with their content (remember to change `fullName` and `googleAccountName` values in `autoRespond.gs` to your own data).
3. Go to File and open "Project properties".
4. Go to "Script properties" tab, click "Add row" and create `ALREADY_SENT` property with the value of `[]`.

### Gmail Web App
1. Go to Gmail web app.
2. Open Settings and go to "Labels" tab.
3. Create a new label to mark messages that arrived when you were unavailale (e.g. `autoreply`).
4. Type into Gmail search: `to:-{google-account-name}+urgent@gmail.com` (remember to replace the string `{google-account-name}` with your actual account name).
5. Click the down arrow on the right side of the search field ("Show search options").
6. Click "Create filter with this search" link at the bottom of search options.
7. Mark "Skip the Inbox (Archive it)" option and "Apply the label" option (choose the label that you created in step 3).
8. Click "Create filter".
9. Open Settings and go to "Filters and Blocked Addresses" tab.
10. Find the newly created filter, mark it, click "Export" button at the bottom of filters list, save the filter to disk.
11. Delete the filter.

## Enable ReFlex
### Gmail Web App
1. Open Settings and go to "Filters and Blocked Addresses" tab.
2. Import filter.

### Google Apps Script
1. Clear `ALREADY_SENT` script property (set the value to `[]`).
2. Add function `AutoRespond_{date}` (with appropriate date) to `responderDates.gs`.
3. Go to Edit and open "Current project's triggers".
4. Click "No triggers set up. Click here to add one now." link.
5. Add trigger for `AutoRespond_{date}` function with following options: `Time-driven`, `Minutes timer`, `Every minute`.

## Disable ReFlex
### Google Apps Script
1. Go to Edit and open "Current project's triggers".
2. Remove trigger.

### Gmail Web App
1. Open Settings and go to "Filters and Blocked Addresses" tab.
2. Remove filter.
