function AutoRespond(e, targetDate) {
  
  // set full name and Google account name
  var fullName = "Maciej Chmielarz"
  var googleAccountName = "maciej.chmielarz"
  
  // set response message body
  var responseBody = "I am unavailable until " + targetDate + ".<br/>" +
    "If the matter is really urgent and requires my reaction before that date, please send a message to " + googleAccountName + "+urgent@gmail.com. Thank you for your understanding.<br/>" +
    "<br/>" + fullName;

  // read addresses to which autoreply has been already sent  
  var scriptProperties = PropertiesService.getScriptProperties();
  var alreadySentJson = scriptProperties.getProperty('ALREADY_SENT');
  var alreadySent = JSON.parse(alreadySentJson);
  
  // get unread threads
  var threads = GmailApp.search("label:autoreply AND label:unread");
  
  // iterate over unread threads
  for (var i = 0; i < threads.length; i++) {
    var messages = threads[i].getMessages()
    // iterate over messages in every unread thread
    for (var j = 0; j < messages.length; j++) {
      // check if message is unread
      if (messages[j].isUnread()) {
        // send autoreply if it has not been already sent
        var fromAddress = messages[j].getFrom();
        if (alreadySent.indexOf(fromAddress) === -1) {
          var messageSubject = messages[j].getSubject()
          messages[j].reply("", {
            htmlBody: responseBody,
            from: fullName + "<" + googleAccountName + "@gmail.com>",
            subject: "(Autoreply) " + messageSubject
          });
          // add address to already sent
          alreadySent.push(fromAddress)
        }
      }
    }
    // mark as read
    threads[i].markRead(); 
  }
  
  // write addressess to which autoreply has been already sent
  alreadySentJson = JSON.stringify(alreadySent);
  scriptProperties.setProperty('ALREADY_SENT', alreadySentJson);
}